create database rugbyScrum;
use rugbyScrum;

create table scrum_type(
id bigint primary key,
name varchar(200) unique
); 

create table scrum_state(
id bigint primary key,
name varchar(225),
type bigint,
foreign key (type) references scrum_type(id) on delete cascade
);

create table person(
id bigint primary key,
first_name varchar(100) not null,
last_name varchar(100) not null,
phone bigint,
cell_phone bigint,
birth_date date,
email varchar(225) unique,
password varchar(100) not null
);

create table scrum_rol(
id bigint primary key auto_increment,
name varchar(100) not null unique,
state bigint default "6",
foreign key (state) references scrum_state(id) on delete cascade
);


create table board(
board_id bigint primary key auto_increment,
name varchar(50) unique,
description varchar(225)
);

create table scrum_project(
id bigint primary key auto_increment,
name varchar(100) not null,
icon varchar(70) default "fa fa-folder",
client varchar(200) not null,
description varchar(225) not null,
state bigint default "8",
foreign key (state) references  scrum_state(id) on delete cascade
);

create table scrum_permission(
id bigint primary key auto_increment,
name varchar(150) not null
);

create table scrum_project_rol(
id bigint primary key auto_increment,
project_id bigint,
foreign key (project_id) references  scrum_project(id) on delete cascade,
rol bigint, 
foreign key (rol) references scrum_rol(id) on delete cascade,
state_id bigint,
foreign key (state_id) references scrum_state(id) on delete cascade
);

create table scrum_rol_permission(
permission_id bigint auto_increment,
foreign key (permission_id) references scrum_permission(id) on delete cascade,
project_rol_id bigint,
foreign key (project_rol_id) references scrum_project_rol(id) on delete cascade,
state_id bigint,
foreign key (state_id) references scrum_state(id) on delete cascade
);

create table scrum_person_project_rol(
id bigint primary key auto_increment,
person_id bigint,
foreign key (person_id) references  person(id) on delete cascade,
project_rol_id bigint,
foreign key (project_rol_id) references  scrum_project_rol(id) on delete cascade,
state_id bigint,
foreign key (state_id) references scrum_state(id) on delete cascade,
timeline longblob
);

create table priority
(priority_id bigint primary key,
 priority_name varchar(100) not null
);

create table story
(story_id bigint primary key auto_increment,
 story_name varchar(200) not null,
 story_state bigint default "14",
 priority_id bigint default "2",
 project_id bigint,
 foreign key (story_state) references scrum_state(id) on delete cascade,
 foreign key (priority_id) references priority (priority_id) on delete cascade,
 foreign key (project_id) references scrum_project (id) on delete cascade
);

create table story_criteria
(story_criteria_id bigint primary key auto_increment,
 puntuation int default "10",
 description varchar(200) not null,
 state_id bigint,
 story_id bigint,
 foreign key (state_id) references scrum_state (id) on delete cascade,
 foreign key (story_id) references story(story_id ) on delete cascade
);

create table sprint
(sprint_id bigint primary key auto_increment,
 sprint_name varchar(100) not null,
 sprint_description varchar(300) not null,
 sprint_state bigint,
 sprint_start_date date,
 sprint_end_date date,
 foreign key (sprint_state) references scrum_state (id) on delete cascade
);

create table scrum_task
(scrum_task_id bigint primary key auto_increment,
 scrum_task_name varchar(100) not null,
 scrum_task_description varchar(300) not null,
 scrum_task_state bigint, 
 story_id bigint,
 scrum_start_date date,
 scrum_end_date date,
 person_id bigint,
 board_id bigint,
 foreign key (person_id) references scrum_person_project_rol (id) on delete cascade,
 foreign key (board_id) references board (board_id) on delete cascade,
 foreign key (scrum_task_state) references scrum_state (id) on delete cascade,
 foreign key (story_id) references story (story_id) on delete cascade
);

create table sprint_backlog
(sprint_id bigint auto_increment,
 scrum_story_id bigint,
 foreign key (sprint_id) references sprint (sprint_id) on delete cascade,
 foreign key (scrum_story_id) references story (story_id) on delete cascade
);

create table project_board
(project_id bigint,
 board_id bigint,
 foreign key (project_id) references scrum_project (id) on delete cascade,
 foreign key (board_id) references board (board_id) on delete cascade
);

drop database rugbyScrum;