/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import controller.ControllerPerson;
import dataAcces.SessionUtils;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import model.Person;
import controller.ControllerPerson;
import controller.ControllerProject;
import dataAcces.SessionUtils;
import java.util.ArrayList;
import java.util.List;
import model.Project;

/**
 *
 * @author Dumar
 */
@Named(value = "beanMenuPrincipal")
@RequestScoped
public class BeanMenuPrincipal {    
    private Person person= new Person();
    protected ControllerPerson controlPerson;    
    private Project project =new Project();
    private ControllerPerson pp;
    private ControllerProject control;
    private List<Project> List=new ArrayList<Project>();

    
    public BeanMenuPrincipal() {        
        person=loadPerson();
    }  
    
    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public List<Project> getList() {
        this.control=new ControllerProject();
        pp= new ControllerPerson();
        //List=control.all(pp.findForEmail(SessionUtils.getEmail()).getIdPerson());
        List=control.all(person.getIdPerson());
        return List;
    }

    public void setList(List<Project> List) {
        this.List = List;
    }
    public Person getPerson() {
        return person;
    }
    public void setPerson(Person person) {
        this.person = person;
    }
    private Person loadPerson(){
        controlPerson= new ControllerPerson();
        return controlPerson.findForEmail(SessionUtils.getEmail());
    }
    
}
