/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import model.Person;
import controller.ControllerPerson;
import dataAcces.SessionUtils;
/**
 *
 * @author dumr
 */
@Named(value = "beanPerson")
@RequestScoped
public class BeanPerson {
    public BeanPerson() {
    }
}
