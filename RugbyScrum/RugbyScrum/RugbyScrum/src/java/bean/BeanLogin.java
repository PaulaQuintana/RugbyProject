/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import controller.ControllerLogin;
import controller.ControllerProject;
import dataAcces.SessionUtils;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Invitado
 */
@Named(value = "beanLogin")
@SessionScoped
public class BeanLogin implements Serializable {
    private ControllerLogin control;
    private String email;
    private String password;
    
    public BeanLogin() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String Ingresar(long id){
                System.out.print("--------------");

        System.out.print(id);
   control=new ControllerLogin();
   ControllerProject pp=new ControllerProject();
	if (pp.findid(id)!=null) {
		HttpSession session = SessionUtils.getSession();
		session.setAttribute("project", id);
		return "MenuProyecto";
	    } else {
            FacesContext.getCurrentInstance().addMessage(
            null,
	    new FacesMessage(FacesMessage.SEVERITY_WARN,
                "Incorrecto el correo mi SENA y su contraseña",
		"Por favor digie el correo y la contraseña por favor"));
            return "menuPrincipal";
	}    
    }
    public String validateUsernamePassword() {
        control=new ControllerLogin();
	if (control.validate(email, password)) {
            HttpSession session = SessionUtils.getSession();
            session.setAttribute("email", email);
            return "MenuPrincipal";
	} else {
            FacesContext.getCurrentInstance().addMessage(
            null,
            new FacesMessage(FacesMessage.SEVERITY_WARN,
                "Incorrecto el correo mi SENA y su contraseña",
		"Por favor digie el correo y la contraseña por favor"));
		return "index";
	}
    }
    public String logout() {
        HttpSession session = SessionUtils.getSession();
	session.invalidate();
        Maso();
        return "index";
    }
    public String Maso() {
        HttpSession session = SessionUtils.getSession();
	session.removeAttribute("project");
        return "index";
    }
    
}
