package controller;

import dataAcces.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Project;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Dumar
 */
public class ControllerProject extends Conexion{
    public Project obj=null;
	private ResultSet rs;
        
	public ResultSet getRs() {
		return rs;
	}
	public void setRs(ResultSet rs) {
		this.rs = rs;
	}
	
	public ArrayList<Project> all(long id){
		ArrayList<Project> lista=new ArrayList<>();
		String q="SELECT p.id, p.name, p.icon, p.client, p.description, p.state FROM scrum_project p LEFT JOIN scrum_person_project_rol pp on (pp.person_id='"+id+"') inner join scrum_project_rol r on (r.id= pp.project_rol_id) WHERE p.id= r.project_id";
            try{
                rs=findQuery(q);
                while(rs.next()){
	            obj = new Project();
                    obj.setIdProject(rs.getLong("p.id"));
	            obj.setName(rs.getString("p.name"));                    
	            obj.setIcon(rs.getString("p.icon"));
	            obj.setClient(rs.getString("p.client"));                    
	            obj.setDecription(rs.getString("p.description"));
	            obj.setState(rs.getLong("p.state"));
	            lista.add(obj);  
            }
		}catch(SQLException e){
            e.printStackTrace();
        }
		return lista;
    }
    public Project findid(long a){
        obj = new Project();
        String q="Select * from scrum_project where id ="+a+ ";";
        try{
            rs=findQuery(q);
            if(rs.first()){
	            obj = new Project();
	            obj.setIdProject(rs.getLong("id"));
	            obj.setName(rs.getString("name"));                    
	            obj.setIcon(rs.getString("icon"));
	            obj.setClient(rs.getString("client"));                    
	            obj.setDecription(rs.getString("description"));
	            obj.setState(rs.getLong("state"));
            }
		}catch(SQLException e){
            e.printStackTrace();
        }
        return obj;
    }
    public boolean add(Project c){
        if(c!=null){
            try{
            String q="INSERT INTO scrum_project(name, icon, client, description, state) VALUES ('"+c.getName()+"','"+c.getIcon()+"','"+c.getClient()+"','"+c.getDecription()+"','"+8+"')";
            return updateQuery(q);
            }catch(SQLException e){
                e.printStackTrace();
            }   
        }
        return false;
        
    }
     public boolean Mo(Project c){
        try{
	String q="Updata tblCliente set nombreCiudad='' where idCiudad=";
        return updateQuery(q);
        }catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }
    public boolean eli(int c){
        try{
	String q="Delete From scrum_project where id="+c+";";
        return updateQuery(q);
        }catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }
}
