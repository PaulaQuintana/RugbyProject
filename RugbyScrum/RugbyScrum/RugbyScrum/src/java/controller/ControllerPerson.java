/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dataAcces.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Person;

/**
 *
 * @author dumr
 */
public class ControllerPerson extends Conexion{
    private Person person;
    protected ResultSet rs;
    public Person findForEmail(String email) {
       String ps="SELECT id,first_name,last_name,phone,cell_phone,birth_date,email,password FROM person where email='"+email+"';";
        try {
            rs = findQuery(ps);
            if (rs.first()) {
                Person p =new Person();
                p.setIdPerson(rs.getLong("id"));
                p.setName(rs.getString("first_name"));
                p.setLastName(rs.getString("last_name"));
                p.setPhone(rs.getLong("phone"));
                p.setCellPhone(rs.getLong("cell_phone"));
                p.setBirthdate(rs.getDate("birth_date"));
                p.setEmail(rs.getString("email"));
                p.setPassword(rs.getString("password"));
                System.out.print(p.getIdPerson());
                return p;
            }
	} catch (SQLException ex) {
            System.out.println("Login error -->" + ex.getMessage());
            return null;
	}
	return null;
    }
}
