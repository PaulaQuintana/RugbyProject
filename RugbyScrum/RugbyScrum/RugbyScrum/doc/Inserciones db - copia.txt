
INSERT INTO `scrum_type` (`id`, `name`) VALUES ('1', 'Persona'), ('2', 'Persona Proyecto'), ('3', 'Rol'), ('4', 'Proyecto'), ('5', 'Sprint'), ('6', 'Historia'), ('7', 'Tareas'),('8', 'Criterio');

INSERT INTO `scrum_state` (`id`, `name`, `type`) VALUES ('1', 'Activo', '1'), ('2', 'Deshabilitado', '1'), ('3', 'Activo', '2'), ('4', 'En solicitud', '2'), ('5', 'Retirado', '2'), ('6', 'Activo', '3'), ('7', 'Inactivo', '3'), ('8', 'Desarrollando', '4'), ('9', 'Terminado', '4'), ('10', 'Cancelado', '4'), ('11', 'Progreso', '5'), ('12', 'Terminado', '5'), ('13', 'Cancelado', '5'), ('14', 'En Backlog', '6'), ('15', 'Sprint', '6'), ('16', 'Terminado', '6'), ('17', 'Asignada', '7'), ('18', 'No asignada', '7'), ('19', 'Realizada', '8'), ('20', 'No aprobada', '8');

INSERT INTO `person` (`id`, `first_name`, `last_name`, `phone`, `cell_phone`, `birth_date`, `email`, `password`) VALUES ('11011011', 'Ivonne', 'Parrado', '9074598', '3002558964', '1989-11-28', 'ivoo_00@misena.edu.co', '123456789'), ('101010', 'Luisa Maria', 'Gomez Velasquez', '2587946', '3214567520', '1999-01-01', 'luma_01@misena.edu.co', '987654321');

INSERT INTO `scrum_project` (`id`, `name`, `icon`, `client`, `description`, `state`) VALUES ('1', 'Gestion Mercado', 'fa fa-html5', 'Juan fernando perez', 'Crear Una pagina web para la gestion de inventario', '8'), ('2', 'Gestion de Novedades', 'fa fa-list-ul', 'Carlos Gomez', 'Novedar a la gente', '9'), ('3', 'Logistic', 'fa fa-bus', 'Juliana', 'Poder crear log�stica de viajes ', '8');

INSERT INTO `scrum_rol` (`id`, `name`, `state`) VALUES ('1', 'Development', '6'), ('2', 'Master', '6'), ('3', 'Product Owner', '6'), ('4', 'Tester', '6'), ('5', 'Dise�adores', '6');

INSERT INTO `priority` (`priority_id`, `priority_name`) VALUES ('1', 'Extrema'), ('2', 'Alta'), ('3', 'Media'), ('4', 'Baja');

INSERT INTO `sprint` (`sprint_id`, `sprint_name`, `sprint_description`, `sprint_state`, `sprint_start_date`, `sprint_end_date`) VALUES ('1', 'Documentacion', 'analizar el proyecto y crear diagramas', '11', '2017-08-17', '2017-08-27'), ('2', 'Planeacion', 'Organizar los viajes internacionales turisticos', '11', '2017-08-20', '2017-08-31'), ('3', 'Presentaci�n', 'Entregar nuevas propuestas a los clientes', '11', '2017-08-25', '2017-09-05');

INSERT INTO `story` (`story_id`, `story_name`, `story_state`, `priority_id`, `project_id`) VALUES ('1', 'Diagramas', '15', '1', '1'), ('2', 'Publicidad', '15', '2', '1');

INSERT INTO `sprint_backlog` (`sprint_id`, `scrum_story_id`) VALUES ('1', '1'), ('1', '2');

INSERT INTO `scrum_task` (`scrum_task_id`, `scrum_task_name`, `scrum_task_description`, `scrum_task_state`, `story_id`, `scrum_start_date`, `scrum_end_date`) VALUES ('1', 'DCU Vendedor', 'Creaci�n del diagrama del caso de uso para el actor vendedor', '17', '1', NULL, NULL), ('2', 'DCU Proveedor', 'Creaci�n del diagrama del caso de uso para el actor proveedor', '17', '1', NULL, NULL), ('3', 'DCU Administrador', 'Creaci�n del diagrama del caso de uso para el actor administrador', '18', '1', NULL, NULL), ('4', 'Creacion de colores', 'Paleta de colores para el dise�o del proyecto', '17', '2', NULL, NULL), ('5', 'Mini Review', 'Crear un corto para la promocion del proyecto', '18', '2', NULL, NULL);

INSERT INTO `scrum_permission` (`id`, `name`) VALUES ('1', 'Gesti�n Proyecto'), ('2', 'Gesti�n Historias'), ('3', 'Gesti�n Tareas'), ('4', 'Gesti�n Sprint'), ('5', 'Columna finalizada'), ('6', 'Permiso de testear'), ('7', 'Gesti�n Tablero'), ('8', 'Asignar Tareas'), ('9', 'Gesti�n Rol');

INSERT INTO `scrum_project_rol` (`id`, `project_id`, `rol`, `state_id`) VALUES ('1', '1', '3', '6'), ('2', '1', '2', '6'), ('3', '2', '3', '6'), ('4', '2', '5', '6'), ('5', '3', '3', '6'), ('6', '3', '4', '6');

INSERT INTO `scrum_person_project_rol` (`id`, `person_id`, `project_rol_id`, `state_id`, `timeline`) VALUES ('1', '101010', '2', '3', NULL), ('2', '11011011', '3', '4', NULL), ('3', '101010', '3', '4', NULL);

INSERT INTO `story_criteria` (`story_criteria_id`, `puntuation`, `description`, `state_id`, `story_id`) VALUES ('1', '8', 'Se require cumplimiento con las normas apa', '17', '1'), ('2', '9', 'Se debe recalcar el eslogan de la empresa', '18', '2'), ('3', '10', 'Son entendibles los casos de uso', '17', '1');

INSERT INTO `assigment` (`assigment_id`, `assigment_person`, `assigment_task`) VALUES ('1', '1', '1'), ('2', '1', '4');

INSERT INTO `board` (`board_id`, `name`, `description`) VALUES ('1', 'Nuevas', 'La tareas est�n frescas'), ('2', 'Pendientes', 'Tareas que faltan realizare '), ('3', 'Progreso', 'La tarea estar� en un estado de proceso'), ('4', 'Testeo', 'Tareas que est�n en proceso de revisi�n'), ('5', 'Finalizado', '!Por fin�. !Tarea lograda�. ');

INSERT INTO `project_board` (`project_id`, `board_id`) VALUES ('1', '1'), ('1', '2'), ('1', '3'), ('1', '4'), ('1', '5'), ('2', '1'), ('2', '2'), ('2', '3');

INSERT INTO `scrum_rol_permission` (`permission_id`, `project_rol_id`, `state_id`) VALUES ('1', '1', '6'), ('2', '1', '6'), ('3', '1', '6'), ('5', '3', '6');