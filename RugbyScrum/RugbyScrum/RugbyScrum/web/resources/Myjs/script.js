$( document ).ready(function(){
 $('.button-collapse').sideNav({
      menuWidth: 200, // Default is 300
      edge: 'left', // Choose the horizontal origin
      closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
      draggable: true, // Choose whether you can drag to open on touch screens,
      onOpen: function(el) { /* Do Stuff*/ }, // A function to be called when sideNav is opened
      onClose: function(el) { /* Do Stuff*/ }, // A function to be called when sideNav is closed
    }
 );
 $('.modal').modal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: .6, // Opacity of modal background
      inDuration: 400, // Transition in duration
      outDuration: 300, // Transition out duration
      startingTop: '100%', // Starting top style attribute
      endingTop: '5%', // Ending top style attribute
    }
 );
 $('.dropdown-button').dropdown({
      inDuration: 400,
      outDuration: 225,
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      hover: true, // Activate on hover
      gutter:0, // Spacing from edge
      belowOrigin: true, // Displays dropdown below the button
      alignment: 'left', // Displays dropdown with edge aligned to the left of button
      stopPropagation: true // Stops event propagation
    }
 );
 $(':input[type=file]').change( function(event) {
	var tmppath = URL.createObjectURL(event.target.files[0]);
    //get parent using closest and then find img element
	$(this).closest("div").find("img").attr('src',tmppath);
 });
});