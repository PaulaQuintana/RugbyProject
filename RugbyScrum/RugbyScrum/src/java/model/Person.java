package model;

import java.sql.Date;
/**
 *
 * @author Invitado
 */
public class Person {
    
    private long idPerson;
    private String Name;
    private String lastName;
    private long phone;
    private long cellPhone;
    private String birthdate;
    private String email;
    private String password;
    private String Icon;

    public Person() {
    }

    public String getIcon() {
        return Icon;
    }

    public void setIcon(String Icon) {
        this.Icon = Icon;
    }
   
    
    public Person(long idPerson, String Name, String lastName, long phone, long cellPhone, String birthdate, String email, String password) {
        this.idPerson = idPerson;
        this.Name = Name;
        this.lastName = lastName;
        this.phone = phone;
        this.cellPhone = cellPhone;
        this.birthdate = birthdate;
        this.email = email;
        this.password = password;
    }

    public long getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(long idPerson) {
        this.idPerson = idPerson;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public long getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(long cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
