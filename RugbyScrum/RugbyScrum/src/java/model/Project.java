package model;

/**
 *
 * @author Dumar
 */
public class Project {
    private long idProject;
    private String name;
    private String icon="fa fa-diamond";
    private String client;
    private String decription;
    private long State;

    public Project() {
    }

    public long getIdProject() {
        return idProject;
    }

    public void setIdProject(long idProject) {
        this.idProject = idProject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getDecription() {
        return decription;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }

    public long getState() {
        return State;
    }

    public void setState(long State) {
        this.State = State;
    }
}