package dataAcces;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Invitado
 */
public class Conexion {
    
    private Connection con;
    private Statement st;
    private ResultSet rs;
    private String database="rugbyscrum";
    private String url="jdbc:mysql://localhost:3306/"+database;
    private String user="root";
    private String password="";
    
    
    public Conexion() {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection(url, user, password);
        }catch(SQLException e){
            e.printStackTrace();
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }        
    }
    
    private boolean getConnection() throws SQLException{
        if(con!=null){
            return true;
        }
        return false;
    }
     protected ResultSet findQuery(String query)throws SQLException{
          if(getConnection()){
              st=con.createStatement();
              rs=st.executeQuery(query);
          }
      return rs;
   }      
   protected boolean updateQuery(String query)throws SQLException{
       if(getConnection()){
              st=con.createStatement();
              if(st.executeUpdate(query)>=1){
                  return true;
              }
          }
       return false;
   }
}
