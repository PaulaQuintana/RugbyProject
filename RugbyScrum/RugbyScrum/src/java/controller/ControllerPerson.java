/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mysql.jdbc.Statement;
import dataAcces.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Person;

/**
 *
 * @author dumr
 */
public class ControllerPerson extends Conexion{

    private Person person;
    private List<Person> listCiudad=new ArrayList<Person>();
    private PreparedStatement pst;
    protected ResultSet rs;
    public Person findForEmail(String email) {
       String ps="SELECT id,first_name,last_name,phone,cell_phone,birth_date,email,password FROM person where email='"+email+"';";
        try {
            rs = findQuery(ps);
            if (rs.first()) {
                person =new Person();
                person.setIdPerson(rs.getLong("id"));
                person.setName(rs.getString("first_name"));
                person.setLastName(rs.getString("last_name"));
                person.setPhone(rs.getLong("phone"));
                person.setCellPhone(rs.getLong("cell_phone"));
                person.setBirthdate(rs.getString("birth_date"));
                person.setEmail(rs.getString("email"));
                person.setPassword(rs.getString("password"));
                return person;
            }
	} catch (SQLException ex) {
            System.out.println("Login error -->" + ex.getMessage());
            return null;
	}
	return null;
    }
    public boolean addPerson(Person person){
        try{
            String q="INSERT INTO person VALUES ('"+person.getIdPerson()+"','"+person.getName()+"','"+person.getLastName()+"','"+person.getPhone()+"','"+person.getCellPhone()+"','"+person.getBirthdate()+"','"+person.getEmail()+"','"+person.getPassword()+"')";
            return updateQuery(q);
        }catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }
    
}
