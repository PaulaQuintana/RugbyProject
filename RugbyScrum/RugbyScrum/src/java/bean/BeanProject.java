package bean;

import controller.ControllerPerson;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import model.Project;
import controller.ControllerProject;
import dataAcces.SessionUtils;
import java.util.ArrayList;
import java.util.List;
import model.Person;

/**
 *
 * @author Dumar
 */
@Named(value = "beanProject")
@RequestScoped
public class BeanProject {
    private Project project =new Project();
    private ControllerPerson pp;
    private ControllerProject control;
    private List<Project> List=new ArrayList<Project>();

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public List<Project> getList() {
        this.control=new ControllerProject();
        pp= new ControllerPerson();
        List=control.all(pp.findForEmail(SessionUtils.getEmail()).getIdPerson());
        return List;
    }

    public void setList(List<Project> List) {
        this.List = List;
    }
    
    public BeanProject() {
    }
    public String createProject(){
    System.out.print("----------------");
        this.control=new ControllerProject();
        if(control.add(project)){
            return "MenuProyecto";
        }
        control=null;
        return "MenuPrincipal";
    }
}

