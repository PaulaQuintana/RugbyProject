/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;
import java.sql.Date;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import model.Person;
import controller.ControllerPerson;
import dataAcces.SessionUtils;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
/**
 *
 * @author dumr
 */
@Named(value = "beanPerson")
@RequestScoped
public class BeanPerson {
    private Person person= new Person();
    protected ControllerPerson control;
    private String msg;
     private List<Person> listaBeanPerson= new ArrayList<Person>();
  
    
    public Person getPerson() {
        return person;
    }
    public void setPerson(Person person) {
        this.person = person;
    }
       public String getMsg() {
        return msg;
    }
    public BeanPerson() {
    }   
    private Person loadPerson(){
        control= new ControllerPerson();
        return control.findForEmail(SessionUtils.getEmail());
    }

    public List<Person> getListaBeanCiudad() {
        return listaBeanPerson;
    }

    public void setListaBeanCiudad(List<Person> listaBeanCiudad) {
        this.listaBeanPerson = listaBeanCiudad;
    }
    public String add(){
        this.control=new ControllerPerson();
        if(control.addPerson(person)){
            return "person";
        }
        control=null;
        return "Error";
      
                
    }
  
 
  

}
