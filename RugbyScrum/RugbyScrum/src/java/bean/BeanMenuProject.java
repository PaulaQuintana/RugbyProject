/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import controller.ControllerProject;
import dataAcces.SessionUtils;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import model.Project;

/**
 *
 * @author Dumar
 */
@Named(value = "beanMenuProject")
@RequestScoped
public class BeanMenuProject {    
    private Project project =new Project();
    private ControllerProject control;
    public BeanMenuProject() {        
        project=loadProject();
    }
    
    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
    private Project loadProject(){
        control=new ControllerProject();
        return control.findid(SessionUtils.getProject());
    }
}
